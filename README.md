# Readme #

This is the crappiest text editor you will ever see. It is really only useful
if you need to do some quick hacks directly on the device, without transferring
files between a host computer. I called it nano because it uses the same
keyboard commands for saving and exiting, otherwise it is completely different.
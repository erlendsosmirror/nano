/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Constant data
///////////////////////////////////////////////////////////////////////////////
const char leftarrow[] = {27, 91, 68, 0};
const char rightarrow[] = {27, 91, 67, 0};
const char downarrow[] = {27, 91, 66, 0};
const char uparrow[] = {27, 91, 65, 0};
const char backspace[] = {0x08, ' ', 0x08, 0};

const char setcolora[] = {0x1B, 0x5B, '3', '2', ';', '1', 'm', 0};
const char setcolorb[] = {0x1B, 0x5B, '3', '4', ';', '1', 'm', 0};
const char resetcolor[] = {0x1B, 0x5B, '0', 'm', 0};

const char blackonwhite[] = {0x1B, 0x5B, '3', '0', ';', '4', '7', 'm', 0};

const char clearscreen[] = {27, 91, '2', 'J', 27, 91, 'H', 0};

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
// 4KiB Data array storing the current text
char data[64][64];

// Current line x index and y (line)
int curx;
int cury;
int curywindow;
int curoff;

// Index of the last line with content
int lastcontent;

/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <fcntl.h>
#include <unistd.h>		// For write, read and environ
#include <time.h>
#include <simpleprint/simpleprint.h>

#include "fileio.h"
#include "globals.h"
#include "screen.h"

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void SetPosition (int x, int y)
{
	char sx[16];
	char sy[16];

	_itoa (x+1, sx);
	_itoa (y+1, sy);

	char setstring[16] = {0x1B, 0x5B, 0};

	strcat (setstring, sy);
	strcat (setstring, ";");
	strcat (setstring, sx);
	strcat (setstring, "H");

	SimplePrint (setstring);
}

void PrintTitle (void)
{
	int pos = 28 - strlen (savename);

	if (pos < 12)
		pos = 12;

	SetPosition (0, 0);
	SimplePrint ((char*)blackonwhite);
	SimplePrint (" jallanano                    ");
	SetPosition (pos, 0);
	SimplePrint (savename);
	SimplePrint ((char*)resetcolor);
}

void PrintSaveMessage (void)
{
	SetPosition (0, 0);
	SimplePrint ((char*)blackonwhite);
	SimplePrint ("                              ");

	SetPosition (1, 0);
	SimplePrint ("Saving ");
	SimplePrint (savename);
	SimplePrint ((char*)resetcolor);
	SetPosition (1, 0);
}

void PrintInfo (void)
{
	SetPosition (0, SIZEH-1);

	SimplePrint ((char*)blackonwhite);
	SimplePrint ("^X");
	SimplePrint ((char*)resetcolor);
	SimplePrint (" Exit      ");

	SimplePrint ((char*)blackonwhite);
	SimplePrint ("^O");
	SimplePrint ((char*)resetcolor);
	SimplePrint (" Write Out     ");
}

void WriteLine (char *str, int ypos, int xoffset)
{
	// Set position to start of line
	SetPosition (0, ypos);

	// Copy input string
	char tmp[64];
	strncpy (tmp, str, 64);
	tmp[63] = 0;

	// Clamp offset
	if (xoffset > 63)
		xoffset = 63;

	// Add offset
	char *src = tmp + xoffset;

	// Get length
	int len = strlen (src);

	// Set max line length to 30
	int maxlen = SIZEW;

	// If we have an offset, add space for the dollar sign
	if (xoffset)
	{
		SimplePrint ("$");
		maxlen--;
	}

	// If still too long, make space for the right-hand sign
	if (len > maxlen)
		maxlen--;

	// Check again and terminate if too long
	if (len > maxlen)
		src[maxlen] = 0;

	// Print the string
	SimplePrint (src);

	// Add the right-hand sign
	if (len > maxlen)
		SimplePrint ("$");
	else
	{
		// Make shure the line is cleared
		memset (tmp, ' ', 64);
		tmp[maxlen-len] = 0;
		SimplePrint (tmp);
	}
}

void WriteScreen (int startindex, int endindex)
{
	for (int i = startindex, j = 0; i < endindex; i++, j++)
		WriteLine (data[i], 2 + j, (cury == i) ? curoff : 0);
}

void RedrawScreen (int force)
{
	// Static vars
	static int oldcury = -1;
	static int oldcurywindow = -1;

	// Check offset
	CheckOffset ();

	// Check what to redraw: screen, surrounding lines or single line
	if (oldcurywindow != curywindow || force)
	{
		WriteScreen (curywindow, curywindow + SIZEH - 4);
	}
	else if (oldcury != cury)
	{
		if (cury - curywindow > 0)
			WriteLine (data[cury-1], cury - 1 - curywindow + 2, 0);

		WriteLine (data[cury], cury - curywindow + 2, curoff);

		if ((cury - curywindow) < (SIZEH - 4 - 1))
			WriteLine (data[cury+1], cury + 1 - curywindow + 2, 0);
	}
	else
	{
		WriteLine (data[cury], cury - curywindow + 2, curoff);
	}

	// Store old values
	oldcurywindow = curywindow;
	oldcury = cury;

	// Set cursor
	SetPosition (curx - curoff + (curoff?1:0), cury - curywindow + 2);
}

void CheckOffset (void)
{
	if (curoff > curx)
		curoff = curx;
	else if (curx > (curoff + SIZEW - 2))
		curoff = curx - (SIZEW - 2);
}

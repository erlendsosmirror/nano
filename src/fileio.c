/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <string.h>
#include <fcntl.h>
#include <unistd.h>		// For write, read and environ
#include <time.h>
#include <simpleprint/simpleprint.h>

#include "globals.h"	// For access to "data"
#include "screen.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
char *savename;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
int Load (char *filename)
{
	// Store filename for later save
	savename = filename;

	// Clear all data storage
	memset (data, 0, sizeof (data));

	// Open file
	int file = open (filename, O_RDONLY);

	// If it failed it probably does not exist, return success
	if (file < 0)
		return 0;

	// Variables
	char rdbuf[64];
	int currentline = 0;
	int currentchar = 0;
	int nbytes;

	// Main load loop
	do
	{
		// Read up tp 64 characters at a time
		nbytes = read (file, rdbuf, 64);

		// Check for errors or continue
		if (nbytes < 0)
			break;

		// Load each character
		for (int i = 0; i < nbytes; i++)
		{
			if (rdbuf[i] == '\n')
			{
				data[currentline][currentchar] = 0;

				currentchar = 0;
				currentline++;

				if (currentline == 64)
				{
					SimplePrint ("Too many lines in input file\n");
					close (file);
					return 1;
				}
			}
			else
			{
				data[currentline][currentchar] = rdbuf[i];

				currentchar++;
				lastcontent = currentline;

				if (currentchar == 63)
				{
					SimplePrint ("Line too long in input file\n");
					close (file);
					return 1;
				}
			}
		}
	} while (nbytes > 0);

	close (file);

	if (nbytes < 0)
		return PrintError ("nano", filename);
	return 0;
}

void Save (void)
{
	// Print message in the title bar
	PrintSaveMessage ();

	// Force new empty file to save to
	int file = open (savename, O_RDWR | O_TRUNC | O_CREAT,
		S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH);

	// Check
	if (file > 0)
	{
		// Vars
		int lastcontentline = 0;

		// Save each line
		for (int i = 0; i < 64; i++)
		{
			if (data[i][0])
			{
				for (int j = lastcontentline + 1; j < i; j++)
					write (file, "\n", 1);

				write (file, data[i], strlen(data[i]));
				write (file, "\n", 1);
				lastcontentline = i;
			}
		}

		// Done with file
		close (file);
	}
	else
		SimplePrint ("Could not save file");

	// Wait a second
	struct timespec req;
	req.tv_sec = 1;
	req.tv_nsec = 0;
	nanosleep (&req, 0);

	// Re-print the title
	PrintTitle ();

	// Use this to set cursor position properly
	RedrawScreen (0);
}

/*
 * Copyright (c) 2018, Erlend Sveen
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

///////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <simpleprint/simpleprint.h>

#include "term.h"
#include "fileio.h"
#include "globals.h"
#include "screen.h"

///////////////////////////////////////////////////////////////////////////////
// Variables
///////////////////////////////////////////////////////////////////////////////
// Used for interpreting escape sequences (like arrow keys)
int inescape = 0;

///////////////////////////////////////////////////////////////////////////////
// Functions
///////////////////////////////////////////////////////////////////////////////
void Backspace (void)
{
	if (curx)
	{
		// Move last portion of string (safe?)
		strcpy (&data[cury][curx-1], &data[cury][curx]);

		// Decrement x position
		curx--;

		// If we have to shift the display
		if (curx < curoff)
			curoff = curx;

		// Redraw screen
		RedrawScreen (0);
	}
	else if (cury)
	{
		// Get length of target and source
		int targetlen = strlen (data[cury-1]);
		int sourcelen = strlen (data[cury]);

		// Copy
		char cpy[64];
		memcpy (cpy, data[cury], 64);

		// Space in target to copy to
		int nchars = 64 - targetlen;

		// Clamp to number of bytes in source
		if (nchars > sourcelen)
			nchars = sourcelen;

		// Concatenate to target
		memcpy (&data[cury-1][targetlen], cpy, nchars);

		// If there are bytes left in next
		if (nchars != sourcelen)
			strcpy (data[cury], &cpy[nchars]);
		else
		{
			data[cury][0] = 0;

			for (int i = cury; i < 64-2; i++)
				memcpy (data[i], data[i+1], 64);
		}

		// Set position
		cury--;
		curx = targetlen;

		// Check last line
		lastcontent--;

		// Check screen top
		if (cury < curywindow)
			curywindow = cury;

		// Redraw screen
		RedrawScreen (1);
	}
}

void Insert (char c)
{
	// Insert or append the character
	int len = strlen (data[cury]);

	// Ignore input if line is at maximum length
	if (len > 62)
		return;

	// Move last portion
	memmove (&data[cury][curx+1], &data[cury][curx], 64 - curx - 1);

	// Insert
	data[cury][curx] = c;

	// Increment position
	if (curx < 63)
		curx++;

	// Write to display
	RedrawScreen (0);
}

void Newline (void)
{
	// Disallow newlines if file is "full"
	if (data[63][0])
		return;

	// Check for split line
	if (data[cury][curx])
	{
		// Make a copy of the rest of the line
		char cpy[64];
		strcpy (cpy, &data[cury][curx]);

		// Terminate line
		data[cury][curx] = 0;

		// Increment position and set cursor to the left
		cury++;
		curx = 0;

		// Move rest
		for (int i = 64-2; i >= cury; i--)
			memcpy (data[i+1], data[i], 64);

		// Write rest
		strcpy (data[cury], cpy);
	}
	else
	{
		// Check if we can add this line
		if (cury >= 63)
			return;

		// Increment position and set cursor to the left
		cury++;
		curx = 0;

		// Need to move data
		for (int i = 64-2; i >= cury; i--)
			memcpy (data[i+1], data[i], 64);

		// Clear that line
		data[cury][0] = 0;
	}

	// Check last line
	lastcontent++;

	// Redraw screen
	RedrawScreen (1);
}

void ProcessEscape (char recvchar)
{
	if (inescape == 27)
		inescape = recvchar;
	else if (inescape == 91)
	{
		if (recvchar == 53)
		{
			// Page up
			inescape = recvchar;
			return;
		}
		else if (recvchar == 54)
		{
			// Page down
			inescape = recvchar;
			return;
		}
		else if (recvchar == 65)
		{
			// Uparrow
			if (cury)
			{
				cury--;
				int len = strlen (data[cury]);
				if (len < curx)
					curx = len;
				if (cury < curywindow)
					curywindow = cury;
				curoff = 0;

				RedrawScreen (0);
			}
		}
		else if (recvchar == 68 && curx)
		{
			// Leftarrow
			curx--;
			RedrawScreen (0);
		}
		else if (recvchar == 67 && curx < 63)
		{
			// Rightarrow
			curx++;
			int len = strlen (data[cury]);
			if (len < curx)
				curx = len;

			RedrawScreen (0);
		}
		else if (recvchar == 66)
		{
			// Downarrow
			if (cury < 63 && cury < lastcontent)
			{
				cury++;
				curoff = 0;

				int len = strlen (data[cury]);

				if (curx > len)
					curx = len;

				if (cury > curywindow+SIZEH-4-1)
					curywindow = cury-SIZEH+4+1;

				RedrawScreen (0);
			}
		}
		else if (recvchar == 70)
		{
			// End
			curx = strlen (data[cury]);
			RedrawScreen (0);
		}
		else if (recvchar == 72)
		{
			// Home
			curx = 0;
			RedrawScreen (0);
		}

		inescape = 0;
	}
	else
		inescape = 0;
}

void Quit (void)
{
	SetPosition (0, SIZEH-1);
	SimplePrint ((char*)blackonwhite);
	SimplePrint (" Are you shure? Y/n           ");
	SimplePrint ((char*)resetcolor);
	SetPosition (20, SIZEH-1);

	char recvchar;

	while (1)
	{
		if (read (0, &recvchar, 1) != 1)
		{
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 3 * 1000000;
			nanosleep (&req, 0);
		}
		else if (recvchar == 'y' ||
			recvchar == 'Y' ||
			recvchar == '\r')
		{
			SimplePrint ((char*)clearscreen);
			TermExit (0);
		}
		else
		{
			PrintInfo ();
			break;
		}
	}
}

void ProcessInput ()
{
	// Receive character and echo string
	char recvchar;

	// Main loop
	while (1)
	{
		if (read (0, &recvchar, 1) != 1)
		{
			struct timespec req;
			req.tv_sec = 0;
			req.tv_nsec = 3 * 1000000;
			nanosleep (&req, 0);
		}
		else if (inescape)
			ProcessEscape (recvchar);
		else if (recvchar == '\r')
			Newline ();
		else if (recvchar == 0x03)
			SimplePrint ("^C from stdin\n");
		else if (recvchar == 0x1A)
			SimplePrint ("^Z from stdin\n");
		else if (recvchar == 0x0F)
			Save ();
		else if (recvchar == 0x18)
			Quit ();
		else if (recvchar == 0x08 || recvchar == 0x7F)
			Backspace ();
		else if (recvchar == 27)
			// Start of escape sequence
			inescape = recvchar;
		else
			Insert (recvchar);
	}
}

int main (int argc, char **argv)
{
	// Check usage
	if (argc != 2)
	{
		SimplePrint ("Usage: nano <filename>\n");
		return 0;
	}

	// Set some globals
	curx = cury = 0;
	curywindow = 0;
	curoff = 0;
	lastcontent = 0;

	// Try to load the file specified
	if (Load (argv[1]))
		return 0;

	// Terminal setup, required for desktop use
	TermSetup ();

	// Clear the screen, print title, info and contents
	SimplePrint ((char*)clearscreen);
	PrintTitle ();
	PrintInfo ();
	RedrawScreen (1);
	SetPosition (0, 2);

	// Main loop
	ProcessInput ();

	// Done
	TermExit (0);
	return 0;
}
